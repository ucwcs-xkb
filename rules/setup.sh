#!/bin/sh
# setup.sh XORGRULESDIR UCWRULESDIR BASEPREFIX DESTDIR
# On Debian: setup.sh /usr/share/X11/xkb/rules rules base /usr/share/X11/xkb/rules
# On other systems, BASEPREFIX might be xfree86 instead of base.
cat "$1/$3" "$2/ucw.plus" >"$4/ucw"
cat "$1/$3.lst" "$2/ucw.lst.plus" >"$4/ucw.lst"
cp "$1/$3.xml" "$4/ucw.xml"
patch "$4/ucw.xml" < "$2/ucw.xml.diff"
